package com.example.user.weather;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.weather.API.Retrofit;
import com.example.user.weather.DataModel.Data;
import com.example.user.weather.DataModel.MyResponse;
import com.example.user.weather.ListView.DaysAdapter;
import com.example.user.weather.ListView.OneDay;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity {

    final List<OneDay> daysList = new ArrayList<>();
    ListView daysListView;
    DaysAdapter daysAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        daysListView = (ListView) findViewById(R.id.list_view_for_days);
        daysAdapter = new DaysAdapter(getApplicationContext(), daysList);

        daysListView = (ListView) findViewById(R.id.list_view_for_days);

        Retrofit.getAllWeather("Kharkiv", "json", "5", "eeec84ddb03248e9d60659d27376f", new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
//                tv1.setText(myResponse.data.ClimateAverages[0].month[0].name.toString());
//                tv2.setText(myResponse.data.current_condition[0].weatherIconUrl.toString());
                for(int i = 0; i < 5; i++){
                    daysList.add(new OneDay(myResponse.data.weather[i].maxtempC,
                                            myResponse.data.weather[i].mintempC,
                                            myResponse.data.weather[i].hourly[3].weatherDesc[0].value.toString(),
                                            myResponse.data.weather[i].hourly[3].weatherIconUrl[0].value.toString()));
                }
                daysListView.setAdapter(daysAdapter);

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
