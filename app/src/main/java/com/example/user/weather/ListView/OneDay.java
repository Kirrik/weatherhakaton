package com.example.user.weather.ListView;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.weather.R;

/**
 * Created by User on 23.02.2016.
 */
public class OneDay extends Activity {

    public String maxTemp;
    public String minTemp;
    public String weatherDesc;
    public String weatherIcon;

    public OneDay(String maxTemp, String minTemp, String weatherDesc, String weatherIcon) {
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.weatherDesc = weatherDesc;
        this.weatherIcon = weatherIcon;
    }


}
