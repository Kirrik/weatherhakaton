package com.example.user.weather.API;

import com.example.user.weather.DataModel.Data;
import com.example.user.weather.DataModel.MyResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;


//import retrofit.RestAdapter;
//import retrofit.http.GET;
//import retrofit.Callback;
//import retrofit.http.Path;

public class Retrofit {

    private static final String ENDPOINT = "http://api.worldweatheronline.com/premium";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/v1/weather.ashx")
        void getAllWeather(@Query("q") String city,
                           @Query("format") String format,
                           @Query("num_of_days") String num_of_days,
                           @Query("key") String key,
                           Callback<MyResponse> callback);
    }

    static {
        initialize();
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getAllWeather(String city, String format, String num_of_days, String key, Callback<MyResponse> callback) {
        apiInterface.getAllWeather(city, format, num_of_days, key, callback);
    }

}
