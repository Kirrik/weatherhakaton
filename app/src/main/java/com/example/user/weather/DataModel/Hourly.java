package com.example.user.weather.DataModel;

/**
 * Created by User on 23.02.2016.
 */
public class Hourly {

    public String chanceoffog;
    public String chanceoffrost;
    public String chanceofrain;
    public String tempC;
    public String tempF;
    public String time;
    public String FeelsLikeC;
    public String FeelsLikeF;
    public String pressure;
    public String windspeedKmph;


    public WeatherDesc[] weatherDesc;
    public WeatherIconUrl[] weatherIconUrl;

}
