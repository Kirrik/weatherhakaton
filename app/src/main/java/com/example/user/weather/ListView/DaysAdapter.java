package com.example.user.weather.ListView;

import android.content.Context;
import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.weather.DataModel.DownloadImageTask;
import com.example.user.weather.R;

import java.util.List;

public class DaysAdapter extends ArrayAdapter<OneDay> {
    public DaysAdapter(Context context, List<OneDay> objects ) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null){
            itemView = View.inflate(getContext(), R.layout.one_day, null);
        }

        TextView min_temp = (TextView) itemView.findViewById(R.id.min_temp);
        TextView max_temp = (TextView) itemView.findViewById(R.id.max_temp);
        TextView weatherDesc = (TextView) itemView.findViewById(R.id.weather_desc);
        ImageView weather_icon = (ImageView) itemView.findViewById(R.id.weather_icon);

        OneDay oneDay = getItem(position);

        max_temp.setText(oneDay.maxTemp.toString());
        min_temp.setText(oneDay.minTemp.toString());
        weatherDesc.setText(oneDay.weatherDesc.toString());

        new DownloadImageTask((ImageView) itemView.findViewById(R.id.weather_icon)).execute(oneDay.weatherIcon.toString());

        return itemView;
    }
}
