package com.example.user.weather.DataModel;

/**
 * Created by User on 23.02.2016.
 */
public class CurrentCondition {

    public int temp_C;
    public int temp_F;
    public String observation_time;

    public WeatherDesc[] weatherDesc;
    public WeatherIconUrl[] weatherIconUrl;

}
